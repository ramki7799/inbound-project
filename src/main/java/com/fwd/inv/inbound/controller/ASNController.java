package com.fwd.inv.inbound.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fwd.inv.inbound.dto.OrderResponse;
import com.fwd.inv.inbound.service.OrderService;

@RestController
public class ASNController {
	
	@Autowired
	OrderService orderService;

	@GetMapping(path = "/getOrders/{asnNo}")
	public ResponseEntity<OrderResponse> getAppointmentByAppNo(@PathVariable("asnNo") String appNo) {
		try {
			if(!StringUtils.isBlank(appNo))
			{
				OrderResponse response = orderService.getOrdersByAsnNo(appNo);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
			else return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
