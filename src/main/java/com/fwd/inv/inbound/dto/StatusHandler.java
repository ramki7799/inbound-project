package com.fwd.inv.inbound.dto;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class StatusHandler{
	@JsonInclude(JsonInclude.Include.NON_NULL)
    String message;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	String error;
	@JsonInclude(JsonInclude.Include.NON_NULL)
    String statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
