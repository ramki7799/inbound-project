package com.fwd.inv.inbound.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class OrderResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 691165081622651529L;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    private StatusHandler statusHandler;
	@JsonInclude(value = Include.NON_NULL)
	private boolean success;
	private List<OrderDto> orders;
	public StatusHandler getStatusHandler() {
		return statusHandler;
	}
	public void setStatusHandler(StatusHandler statusHandler) {
		this.statusHandler = statusHandler;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public List<OrderDto> getOrders() {
		return orders;
	}
	public void setOrders(List<OrderDto> orders) {
		this.orders = orders;
	}
	
	
	
	
	
	
}
