package com.fwd.inv.inbound.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.fwd.inv.inbound.constants.ASNServiceConstants;
import com.fwd.inv.inbound.dto.OrderDto;
import com.fwd.inv.inbound.dto.OrderResponse;
import com.fwd.inv.inbound.dto.StatusHandler;
import com.fwd.inv.inbound.entities.OrderEntity;
import com.fwd.inv.inbound.mappers.AsnMapper;
import com.fwd.inv.inbound.repositories.OrderRepository;
import com.fwd.inv.inbound.service.OrderService;

public class OrderServiceImpl implements OrderService{
	
	@Autowired
	OrderRepository repository;
 

	@Override
	public OrderResponse getOrdersByAsnNo(String asnNo) {
		
		OrderResponse response = new OrderResponse();
		StatusHandler statusHandler = new StatusHandler();
		
	try {
		List<OrderEntity> orderEntity = repository.findByAsnNo(asnNo);
		return getAppointmentResponse(orderEntity);	
	}
	catch (Exception e) {
		statusHandler.setError(e.getMessage());
		statusHandler.setStatusCode(ASNServiceConstants.INTERNAL_SERVER_ERROR_CD);
		response.setStatusHandler(statusHandler);
		return response;
	}
	}
	
	public static OrderResponse getAppointmentResponse(List<OrderEntity> orderEntity)
	{
		StatusHandler statusHandler = new StatusHandler();
		OrderResponse response = new OrderResponse();
		List<OrderDto> orderDtos = AsnMapper.mapOrderEntityToDomain(orderEntity);
		if(orderDtos.isEmpty())
		{
			statusHandler.setMessage(ASNServiceConstants.NO_APPOINTMENTS);
        	statusHandler.setStatusCode(ASNServiceConstants.NO_CONTENT);
		}
		else statusHandler.setStatusCode(ASNServiceConstants.STATUS_CD_SUCCESS);
		response.setSuccess(true);
		response.setStatusHandler(statusHandler);
		response.setOrders(orderDtos);
		return response;	
	}

	}
	
	


