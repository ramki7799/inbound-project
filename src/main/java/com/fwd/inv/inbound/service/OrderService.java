package com.fwd.inv.inbound.service;

import com.fwd.inv.inbound.dto.OrderResponse;

public interface OrderService {

	OrderResponse getOrdersByAsnNo(String asnNo);
}
