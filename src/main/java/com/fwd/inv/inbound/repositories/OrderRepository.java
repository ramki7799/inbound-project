package com.fwd.inv.inbound.repositories;

import java.util.List;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.fwd.inv.inbound.entities.OrderEntity;

@Repository
public interface OrderRepository extends CassandraRepository<OrderEntity, Integer>{

	@AllowFiltering
	 List<OrderEntity> findByAsnNo(String asnNo);
}
