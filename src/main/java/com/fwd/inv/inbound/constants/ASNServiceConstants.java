package com.fwd.inv.inbound.constants;

public class ASNServiceConstants {

	public static final String CHECKIN_SERVICE_TOPIC = "checkin-service-topic";
	public static final String NO_APPOINTMENTS = "No Appointments";
	public static final int MIN_LENGTH = 100000000;
	public static final int MAX_LENGTH = 900000000;
	public static final String INTERNAL_SERVER_ERROR_MSG = "Internal Server Error";
	public static final String INTERNAL_SERVER_ERROR_CD = "500";
	public static final String SUCCESSFULLY_DOCK_ASSIGNED = "Dock Door Assigned Successfully";
	public static final String CHECKIN_PRODUCE_FAILED = "Error While Producing Checkin";
	public static final String DOCK_ASSIGNED_FAILED = "Failure While Assigning Dock Door";
	public static final String MISSING_FEW_FIELDS = "Missing Few Fields";
	public static final String NO_CONTENT = "204";
	public static final String BAD_REQUEST_CD = "400";
	public static final String SAVED_SUCCESS = "Successfully Saved";
	public static final String SAVE_FAILED = "Failure while saving";
	public static final String UPDATE_SUCCESS = "Successfully updated";
	public static final String STATUS_CD_SUCCESS = "200";
	public static final String APPOINTMENT_NOT_EXISTS = "Appointment Not Exists";
	public static final String PROVIDE_THE_MANDATORY_FIELDS = "Provide The Mandatory Details";
	public static final String TRAILER_CHECKED_IN = "Trailer Checked-In";




}
