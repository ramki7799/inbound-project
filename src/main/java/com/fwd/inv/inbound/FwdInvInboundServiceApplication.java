package com.fwd.inv.inbound;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FwdInvInboundServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FwdInvInboundServiceApplication.class, args);
	}

}
