package com.fwd.inv.inbound.mappers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.fwd.inv.inbound.dto.OrderDto;
import com.fwd.inv.inbound.entities.OrderEntity;

public class AsnMapper {
	
	public static List<OrderDto> mapOrderEntityToDomain(List<OrderEntity> orderEntitities)
	{
		List<OrderDto> orders = new ArrayList<>();
		
		for(OrderEntity orderEntity:orderEntitities)
		{
			OrderDto order = new OrderDto();
		BeanUtils.copyProperties(orderEntity, order);
		orders.add(order);
		}
		return orders;
	}

}
