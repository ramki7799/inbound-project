package com.fwd.inv.inbound.entities;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("T_ORDER")
public class OrderEntity implements Serializable{

	@PrimaryKey
	  private int shipmentNum;
	  private int modelNum;
	  private int custSalesOrder;
	  private int itemNum;
	  private int channelCode;
	  private int shipDate;
	  private String routeName;
	  private String routeFlag;
	  private String orderStatus;
	  private String orderType;
	  private LocalDate orderDate;
	  private String productType;
	  private int productWeight;
	  private int quantity;
	  private int modelSeq;
	  private String vendor;
	  private String shipmentStatus;
	  private String lstShipmentStatus;
	  private int truckNum;
	public int getShipmentNum() {
		return shipmentNum;
	}
	public void setShipmentNum(int shipmentNum) {
		this.shipmentNum = shipmentNum;
	}
	public int getModelNum() {
		return modelNum;
	}
	public void setModelNum(int modelNum) {
		this.modelNum = modelNum;
	}
	public int getCustSalesOrder() {
		return custSalesOrder;
	}
	public void setCustSalesOrder(int custSalesOrder) {
		this.custSalesOrder = custSalesOrder;
	}
	public int getItemNum() {
		return itemNum;
	}
	public void setItemNum(int itemNum) {
		this.itemNum = itemNum;
	}
	public int getChannelCode() {
		return channelCode;
	}
	public void setChannelCode(int channelCode) {
		this.channelCode = channelCode;
	}
	public int getShipDate() {
		return shipDate;
	}
	public void setShipDate(int shipDate) {
		this.shipDate = shipDate;
	}
	public String getRouteName() {
		return routeName;
	}
	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}
	public String getRouteFlag() {
		return routeFlag;
	}
	public void setRouteFlag(String routeFlag) {
		this.routeFlag = routeFlag;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public int getProductWeight() {
		return productWeight;
	}
	public void setProductWeight(int productWeight) {
		this.productWeight = productWeight;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getModelSeq() {
		return modelSeq;
	}
	public void setModelSeq(int modelSeq) {
		this.modelSeq = modelSeq;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getShipmentStatus() {
		return shipmentStatus;
	}
	public void setShipmentStatus(String shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}
	public String getLstShipmentStatus() {
		return lstShipmentStatus;
	}
	public void setLstShipmentStatus(String lstShipmentStatus) {
		this.lstShipmentStatus = lstShipmentStatus;
	}
	public int getTruckNum() {
		return truckNum;
	}
	public void setTruckNum(int truckNum) {
		this.truckNum = truckNum;
	}
	  
	  
}
